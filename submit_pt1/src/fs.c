#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include "fs.h"

int main() {
    char buff[MAX_INPUT_LEN];
    char ** argv;
    uint32_t argc;

    dir_t * dir;
    file_t * tmp;
    uint16_t i;
    int rc;

    /* init a directory */
    dir = (dir_t *) malloc (sizeof(dir_t));
    if(init_dir(dir) == -1) {
	fprintf(stderr, "unable to init a directory\n");
	exit(1);
    }
    
    srand(time(NULL));

    /* generate n files specified by FILE_COUNT*/
    for( i=1; i <= FILE_COUNT; i++ ) {
	tmp = (file_t *) malloc (sizeof(file_t));
	if( generate_file( i, tmp ) == -1 ) {
	    fprintf(stderr, "unable to generate file %u\n", i);
	    exit(1);
	}
	
	if( add_file(dir, tmp) != 0 ) {
	    fprintf(stderr, "unable to add file to directory %u\n", i);
	    exit(1);
	}
	dir->count++;
    }
        
    /* Run until user exits */
    while(TRUE) {
	printf(">>> ");

	while( (rc =  get_line(buff, sizeof(buff))) <= 0 ) {
	    if( rc == -1 || rc == 0) printf("No input\n> ");
	    if( rc == -2 ) printf("Input too long\n> ");
	}

	command c = get_command(buff, &argc, &argv);

	switch(c) {
	case LIST:
	    list(dir);
	    break;
	case CREATE:
	    create_file( dir, argc, argv );
	    break;
	case RENAME:
	    rename_file( dir, argc, argv );
	    break;
	case DELETE:
	    delete_file( dir, argc, argv );
	    break;
	case HELP:
	    print_help();
	    break;
	case EXIT:
	    printf("exitting...\n");
	    exit(0);
	default:
	    printf("invalid command. type help for list of commands\n");
	}

    }

    return 0;
}

/* function to run for list command */
void list( dir_t * dir ) {
    printf("File Name\tSize(blocks)\tSize(bytes)\n");
    print_dir(dir);
}

/* creates a file in the directory */
int create_file( dir_t * dir, uint32_t argc , char ** argv ) {
    uint32_t fd, size, i;

    file_t * tmp;
    
    if( argc != 3 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: create FD SIZE\n");
	return(-1);
    }

    if((fd = atoi(argv[1])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if( file_exists( dir, fd ) ) {
	fprintf(stderr, "identifier must be unique\n");
	return(-1);
    }

    if((size = atoi(argv[2])) == 0 ) {
	fprintf(stderr, "invalid size\n");
	return(-1);
    }

    if( size > MAX_BLOCKS || size < 1 ) {
	fprintf(stderr, "invalid size\n" );
	return(-1);
    }

    tmp = (file_t *) malloc (sizeof(file_t));
    tmp->fd = fd;

    for( i=0; i<size; i++ ) {
	tmp->block[i] = (struct block_t *) malloc (sizeof(struct block_t));
	/* attempt to fill block with random data */
	if( fill_block(tmp->block[i]) == -1 ) {
	    fprintf(stderr, "unable to fill block %u in file %u\n", i, fd);
	    exit(1);
	}
	tmp->blocks_used++;
    }

    if( add_file(dir, tmp) != 0 ) {
	fprintf(stderr, "unable to add file to directory %u\n", i);
	exit(1);
    }
    dir->count++;

    
    return 0;
}

/* renames a file by file descriptor in the directory */
int rename_file( dir_t * dir, uint32_t argc, char ** argv ) {
    uint32_t fd, new_fd;
    file_t * file;

    if( argc != 3 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: rename FILE NEW\n");
	return(-1);
    }

    if((fd = atoi(argv[1])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if((new_fd = atoi(argv[2])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if( file_exists( dir, new_fd ) ) {
	fprintf(stderr, "new identifier must be unique\n");
	return(-1);
    }

    if( (file = get_file( dir, fd )) == NULL ) {
	fprintf(stderr, "file with this descriptor does not exist\n");
	return(-1);
    }

    file->fd = new_fd;
    return 0;
}

/* deletes files */
int delete_file( dir_t * dir, uint32_t argc, char ** argv ) {
    uint32_t fd,i;

    if( argc < 2 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: delete FILE...\n");
	return(-1);
    }
    
    for( i=1; i<argc; i++ ) {
	fd = atoi(argv[i]);
	remove_file( dir, fd );
    }
    return 0;
}


void print_help   () {
    printf("help\t\tPrints this list\n");
    printf("list\t\tLists files in this directory\n");
    printf("create FILE\tcreates file\n");
    printf("rename FILE NEW\trenames file\n");
    printf("delete FILE...\tdeletes files\n");
    printf("exit\t\tExits the program\n");

}

/* read input from user with overflow protection */
int get_line( char * buff, size_t sz) {
    uint16_t len;
    char ch;
    
    /* check if there was input, otherwise report error */
    if( fgets(buff,sz,stdin) == NULL )
	return(-1);

    len = strlen(buff);

    /* check if the last char is '\n' */
    if( buff[len-1] != '\n' ) {
	/* if it isn't, but the next char is, return */
	if( (ch = getchar()) == '\n' )
	    return len;

	/* Otherwise flush to EOF and return error code */
	while ( (ch = getchar()) != '\n' && (ch != EOF));
	return(-2);
    }

    /* If the input was less than the buffer, swap the '\n' for '\0' and return len of string */
    buff[len-1] = '\0';
    return len-1;
}

/* Allocates memory for a file struct and fills it with random number of blocks up to MAX_BLOCKS */
int generate_file( uint32_t fd, file_t * file ) {
    uint16_t num_blocks;
    uint16_t i;

    file->fd = fd;

    num_blocks = (uint16_t) 1+(rand() % (MAX_BLOCKS-1));
    file->blocks_used = 0;

    for( i=0; i<num_blocks; i++ ) {
	file->block[i] = (struct block_t *) malloc (sizeof(struct block_t));
	/* attempt to fill block with random data */
	if( fill_block(file->block[i]) == -1 ) {
	    fprintf(stderr, "unable to fill block %u in file %u\n", i, fd);
	    exit(1);
	}
	file->blocks_used++;
    }

    return(0);
}

/* Fills a block of memory with random data */
int fill_block( block_t * block ) {
    uint16_t i;

    /* fill block with random bytes */
    for( i=0; i<BLOCK_SIZE; i++ )
	block->b[i] = (byte_t)rand();

    return(0);
}

/* returns 1 if file descriptor exists in directory already */
int file_exists( dir_t * dir, uint32_t fd ) {
    if( get_file( dir, fd ) == NULL )
	return 0;
    return 1;
}
