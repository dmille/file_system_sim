/* Header for 801fs.c */

/* Some useful definitions */
#define BLOCK_SIZE 512
#define MAX_BLOCKS 40
#define MAX_FILENAME_SIZE 32
#define MAX_FILES 64
#define FILE_COUNT 10
#define FALSE 0
#define TRUE 1
#define MAX_INPUT_LEN 16
#define BAD_KEY -1
#define MAX_ARGS 10

/* enumerate the commands */
typedef enum { LIST, CREATE, RENAME, DELETE, EXIT, HELP } command;

/* key - value pair struct */
typedef struct { char *key; command val; } kv_pair;

/* look up table for commands from string */
static kv_pair lookup_table[] = {
    {"help", HELP},
    {"h", HELP},
    {"list", LIST},
    {"ls", LIST},
    {"create", CREATE},
    {"touch", CREATE},
    {"rename", RENAME},
    {"mv", RENAME},
    {"delete", DELETE},
    {"rm", DELETE},
    {"exit", EXIT},
    {"q", EXIT}
};

#define NKEYS (sizeof(lookup_table)/sizeof(kv_pair))

/* refedefine some types for ease of use */
typedef uint8_t byte_t;
typedef byte_t bool;

/* struct defining a block */
typedef struct block_t { byte_t b[BLOCK_SIZE]; } block_t;

/* struct defining a file */
typedef struct file_t {
    uint16_t blocks_used; 
    uint32_t fd; /* file descriptor */
    block_t * block[MAX_BLOCKS];
    struct file_t * next;
} file_t;

/* struct defining a directory */
typedef struct dir_t {
    uint16_t count;
    file_t * file[MAX_FILES];
    struct file_t * first;
    struct file_t * last;
} dir_t;

int fill_block( block_t * block );
int generate_file( uint32_t fnum, file_t * file );
void print_file_data( file_t * file );
command get_command( char * key, uint32_t * argc, char ** arg[] );
int get_line( char * buff, size_t sz );
int file_exists( dir_t * dir, uint32_t fd );

void list( dir_t * dir );
int create_file( dir_t * dir, uint32_t argc, char ** arg );
int rename_file( dir_t * dir, uint32_t argc, char ** arg );
int delete_file( dir_t * dir, uint32_t argc, char ** arg );
void print_help();

/* get the command and arg from user input string */
command get_command( char * input, uint32_t * argc, char ** argv[] ) {
    uint32_t i,j;
    char * s;
    char * tok;
    char str[MAX_INPUT_LEN];
    const char * delim = " ";

    strcpy(str,input);
    s = str;    

    (*argc) = 1;

    /* count number of args (whitespace delimited) */
    for( i = 0; i<MAX_INPUT_LEN; i++ ) {
	if(str[i] == ' ')
	    (*argc)++;
    }
    
    /* get first token of string */
    tok = strsep(&s, delim);

    /* allocate space for the argument vector */
    (*argv) = malloc(sizeof(char*) * (*argc));
    (*argv)[0] = malloc(sizeof(tok));
    strcpy((*argv)[0],tok);
    j=1;
    /* find the matching key */
    for( i = 0; i<NKEYS; i++ ) {
	kv_pair entry = lookup_table[i];
	if( strcmp(entry.key, tok) == 0 ) {
	    /* If there's an argument get it and return in arg */
	    while( (tok = strsep(&s, delim)) != NULL) {
		(*argv)[j] = malloc(sizeof(tok));
		strcpy((*argv)[j],tok);
		j++;
	    }
	    return entry.val;
	}
    }
    /* if there were no matches, return BAD_KEY */
    (*argc) = 0;
    return BAD_KEY;
}

/* Linked List */
int init_dir( struct dir_t * dir ) {
    dir->count = 0;
    dir->first = NULL;
    dir->last = NULL;
    return 0;
}

/* add page to front of queue */
int add_file( struct dir_t * dir, struct file_t * new ) {
    /* if dir is empty, set both the first and last pointer to the new file and return */
    if( dir->first == NULL ) {
	dir->first = new;
	dir->last = new;
	new->next = NULL;
	return 0;
    }

    /* otherwise point the old last element's pointer to new, point last to new, and point new to NULL */
    dir->last->next = new;
    dir->last = new;
    new->next = NULL;
    return 0;
}

struct file_t * get_file( dir_t * dir, uint16_t fd ) {
    struct file_t * it;
    it = dir->first;
    while( it != NULL ) {
	if( it->fd == fd )
	    return it;
	it = it->next;
    }
    return NULL;
}

/* removes page from queue and returns removed page */
struct file_t * remove_file( struct dir_t * dir, uint32_t fd ) {
    struct file_t * it;
    struct file_t * tmp;

    it = dir->first;

    /* If there are no files */
    if( it == NULL ) {
	fprintf(stderr, "No such file\n");	
	return(NULL);
    }
    
    /* if the first file is the correct file */
    if( it->fd == fd ) {
	if( dir->last == it ) {
	    dir->last == NULL;
	    dir->first == NULL;
	}
	dir->first = it->next;
	return it;
    }
    
    /* all other cases */
    while( it->next != NULL ) {
	if( it->next->fd == fd ) {
	    tmp = it->next;
	    it->next = it->next->next;
	    if( it->next == NULL ) {
		dir->last = it;
	    }
	    return tmp;
	}
	it = it->next;
    }
    
    /* if the file didn't exist */
    fprintf(stderr, "No such file\n");
    return(NULL);
}

/* Prints all file data in the directory */
void print_dir(struct dir_t * dir) {
    struct file_t * it;

    it = dir->first;

    while(it != NULL) {	
	print_file_data( it );
	it = it->next;
    }
}

/* prints data for file */
void print_file_data( file_t * file ) {
    printf("%u\t\t%u\t\t%u\n", file->fd, file->blocks_used, file->blocks_used * BLOCK_SIZE);
}
