# README #

### CPS801 - Advanced Operating Systems Final Project ###

* The full description of the project and requirements may be read in project.pdf

### Compiling ###

Run the makefile as follows:

```
#!bash
make

```
This will produce a binary called fs. Run the program as follows:
```
#!bash
./fs
```
This will start a prompt. Type 'help' for available commands