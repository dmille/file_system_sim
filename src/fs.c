#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include "fs.h"
#include "disk.h"

int main() {
    disk_cont_t * cont_disk;
    disk_noncont_t * noncont_disk;
    dir_t * dir;

    /* init the disk */
    cont_disk = (disk_cont_t *) malloc (sizeof(disk_cont_t));
    noncont_disk = (disk_noncont_t *) malloc (sizeof(disk_noncont_t));
    init_noncont_disk(noncont_disk);

    /* init a directory */
    dir = (dir_t *) malloc (sizeof(dir_t));
    if(init_dir(dir) == -1) {
	fprintf(stderr, "unable to init a directory\n");
	exit(1);
    }
    
    srand(time(NULL));

    prompt(dir,cont_disk,noncont_disk);
    return 0;
}


/* generate 10 files in the directory */
int generate_dir( dir_t * dir ) {
    int i;
    file_t * tmp;

    /* generate n files specified by FILE_COUNT*/
    for( i=1; i <= FILE_COUNT; i++ ) {
	tmp = (file_t *) malloc (sizeof(file_t));
	if( generate_file( i, tmp ) == -1 ) {
	    fprintf(stderr, "unable to generate file %u\n", i);
	    return(1);
	}
	
	if( add_file(dir, tmp) != 0 ) {
	    fprintf(stderr, "unable to add file to directory %u\n", i);
	    return(1);
	}
	dir->count++;
    }
    return 0;
}

int prompt(dir_t * dir, disk_cont_t * cont_disk, disk_noncont_t * noncont_disk ) {
    int rc;
    char buff[MAX_INPUT_LEN];
    char ** argv;
    uint32_t argc;

    /* Run until user exits */
    while(TRUE) {
	printf(">>> ");

	while( (rc =  get_line(buff, sizeof(buff))) <= 0 ) {
	    if( rc == -1 || rc == 0) printf("No input\n> ");
	    if( rc == -2 ) printf("Input too long\n> ");
	}

	command c = get_command(buff, &argc, &argv);

	switch(c) {
	case LIST:
	    list(dir);
	    break;
	case CREATE:
	    create_file( dir, argc, argv );
	    break;
	case RENAME:
	    rename_file( dir, argc, argv );
	    break;
	case DELETE:
	    delete_file( dir, argc, argv );
	    break;
	case HELP:
	    print_help();
	    break;
	case DISK:
	    disk_op( cont_disk, noncont_disk, dir, argc, argv );
	    break;
	case EXIT:
	    printf("exitting...\n");
	    return(-1);
	case GEN:
	    generate_dir( dir );
	    break;
	case READ:
	    read_files_in( dir, argc, argv );
	    break;
	case WRITE:
	    write_files_out( dir, argc, argv );
	    break;
	default:
	    printf("invalid command. type help for list of commands\n");
	}

    }

    return 0;

}

int disk_op( disk_cont_t * cont_disk, disk_noncont_t * noncont_disk, dir_t * dir, uint32_t argc, char ** argv ) {
    int fd;
    file_t * file;
    bool cont;

    if( argc < 3 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: disk [cont,noncont] [read,write,delete,list] FD\n");	
	return(-1);
    } else {
	if( strcmp(argv[1], "cont" ) == 0 )
	    cont = TRUE;
	else if( strcmp(argv[1], "noncont" ) == 0 )
	    cont = FALSE;
	else {
	    fprintf(stderr, "must specify whether disk is contiguous or not\n");
	    return(-1);
	}
    }

    /* LIST DISK */
    if( argc == 3 ) {
	if( strcmp(argv[2], "list" ) == 0) {
	    if( cont )
		list_cont( cont_disk );
	    else
		list_noncont( noncont_disk );
	    return(0);
	}
    }



    if( argc != 4) {
	printf("Invalid number of arguments\n");
	printf("Usage: disk [cont,noncont] [read,write,delete,list] FD\n");	
	return(-1);
    }

    /* RUN DISK OPERATIONS */
    if( strcmp(argv[2], "oplist" ) == 0) {
	if( cont )
	    diskop_cont( cont_disk, dir, argv[3] );
	else
	    diskop_noncont( noncont_disk, dir, argv[3] );
	return(0);
    }

    /* WRITE TO DISK */
    if( strcmp(argv[2], "write" ) == 0) {
	if( strcmp(argv[3], "all" ) == 0) {
	    if( cont )
		write_all_cont( cont_disk, dir );
	    else
		write_all_noncont( noncont_disk, dir );
	    return(0);
	} 

	if((fd = atoi(argv[3])) == 0 ){
	    fprintf(stderr, "invalid identifier\n");
	    return(-1);
	}
	if( cont )
	    write_cont( cont_disk, get_file(dir, fd));	
	else
	    write_noncont( noncont_disk, get_file(dir, fd));
	return(0);
    }

    /* DELETE FROM DISK */
    if( strcmp(argv[2], "delete" ) == 0) {
	/* if( strcmp(argv[2], "all" ) == 0) { */
	/*     delete_all( cont_disk, dir ); */
	/*     return(0); */
	/* }  */

	if((fd = atoi(argv[3])) == 0 ){
	    fprintf(stderr, "invalid identifier\n");
	    return(-1);
	}
	if( cont )
	    delete_cont( cont_disk, fd );		
	else
	    delete_noncont( noncont_disk, fd );
	return(0);
    }

    /* READ FROM DISK */
    if( strcmp(argv[2], "read" ) == 0) {
	if((fd = atoi(argv[3])) == 0 ){
	    fprintf(stderr, "invalid identifier\n");
	    return(-1);
	}
	
	if( cont ) {
	    if( (file = read_cont( cont_disk, fd )) == NULL) {
		fprintf(stderr, "file not found on disk\n");
		return(-1);
	    }
	}
	else {
	    if( (file = read_noncont( noncont_disk, fd )) == NULL ) {
		fprintf( stderr, "file not found on disk\n");
		return(-1);
	    }
	}
	
	printf("File Name\tSize(blocks)\tSize(bytes)\n");
	print_file_data( file );
	return(0);
    }


    return(0);
}

int write_files_out( dir_t * dir, uint32_t argc, char ** argv ) {
    FILE * fout;

    struct file_t * it;

    printf("%s\n", argv[1]);
    if( argc != 2 ) {
	fprintf(stderr, "incorrect number of arguments\n");
	return(-1);
    }

    if( (fout = fopen( argv[1], "w" )) == NULL ) {
	fprintf(stderr, "unable to open file\n");
	return(-1);
    }
    
    it = dir->first;
    while( it != NULL ) {
	fprintf(fout,"%u %u %u\n", it->fd, it->blocks_used, it->blocks_used * BLOCK_SIZE);
	it = it->next;
    }

    fclose(fout);
    return 0;
}


int read_files_in( dir_t * dir, uint32_t argc, char ** argv ) {
    FILE * fin;
    char * line = NULL;
    size_t len;
    ssize_t read;
    uint32_t count;
    char ** prop;

    printf("%s\n", argv[1]);
    if( argc != 2 ) {
	fprintf(stderr, "incorrect number of arguments\n");
	return(-1);
    }

    if( (fin = fopen( argv[1], "r" )) == NULL ) {
	fprintf(stderr, "unable to read file\n");
	return(-1);
    }
    
    while( ( read = getline( &line, &len, fin )) != -1 ) {
	get_args( line, &count, &prop );
	printf("fd:%s, size:%s, size:%s\n", prop[0],prop[1],prop[2]);
	strcpy(prop[2], prop[1]);
	strcpy(prop[1], prop[0]);
	create_file(dir, count, prop );
    }
    fclose(fin);
    return 0;
}

/* write all files in directory to disk */
void write_all_cont( disk_cont_t * disk, dir_t * dir ) {
    struct file_t * it;
    it = dir->first;
    while(it != NULL) {
	write_cont( disk, it );
	it = it->next;
    }
}

void write_all_noncont( disk_noncont_t * disk, dir_t * dir ) {

}

/* function to run for list command */
void list( dir_t * dir ) {
    printf("File Name\tSize(blocks)\tSize(bytes)\n");
    print_dir(dir);
}

/* creates a file in the directory */
int create_file( dir_t * dir, uint32_t argc , char ** argv ) {
    uint32_t fd, size, i;

    file_t * tmp;
    
    if( argc != 3 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: create FD SIZE\n");
	return(-1);
    }

    if((fd = atoi(argv[1])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if( file_exists( dir, fd ) ) {
	fprintf(stderr, "identifier must be unique\n");
	return(-1);
    }

    if((size = atoi(argv[2])) == 0 ) {
	fprintf(stderr, "invalid size\n");
	return(-1);
    }

    if( size > MAX_BLOCKS || size < 1 ) {
	fprintf(stderr, "invalid size\n" );
	return(-1);
    }

    tmp = (file_t *) malloc (sizeof(file_t));
    tmp->fd = fd;

    for( i=0; i<size; i++ ) {
	tmp->block[i] = (struct block_t *) malloc (sizeof(struct block_t));
	/* attempt to fill block with random data */
	if( fill_block(tmp->block[i]) == -1 ) {
	    fprintf(stderr, "unable to fill block %u in file %u\n", i, fd);
	    exit(1);
	}
	tmp->blocks_used++;
    }

    if( add_file(dir, tmp) != 0 ) {
	fprintf(stderr, "unable to add file to directory %u\n", i);
	exit(1);
    }
    dir->count++;

    
    return 0;
}

/* renames a file by file descriptor in the directory */
int rename_file( dir_t * dir, uint32_t argc, char ** argv ) {
    uint32_t fd, new_fd;
    file_t * file;

    if( argc != 3 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: rename FILE NEW\n");
	return(-1);
    }

    if((fd = atoi(argv[1])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if((new_fd = atoi(argv[2])) == 0 ){
	fprintf(stderr, "invalid identifier\n");
	return(-1);
    }

    if( file_exists( dir, new_fd ) ) {
	fprintf(stderr, "new identifier must be unique\n");
	return(-1);
    }

    if( (file = get_file( dir, fd )) == NULL ) {
	fprintf(stderr, "file with this descriptor does not exist\n");
	return(-1);
    }

    file->fd = new_fd;
    return 0;
}

/* deletes files */
int delete_file( dir_t * dir, uint32_t argc, char ** argv ) {
    uint32_t fd,i;

    if( argc < 2 ) {
	printf("Invalid number of arguments\n");
	printf("Usage: delete FILE...\n");
	return(-1);
    }
    
    for( i=1; i<argc; i++ ) {
	fd = atoi(argv[i]);
	remove_file( dir, fd );
    }
    return 0;
}


void print_help   () {
    printf("help\t\tPrints this list\n");
    printf("list\t\tLists files in this directory\n");
    printf("create FILE\tcreates file\n");
    printf("rename FILE NEW\trenames file\n");
    printf("delete FILE...\tdeletes files\n");
    printf("exit\t\tExits the program\n");

}

/* read input from user with overflow protection */
int get_line( char * buff, size_t sz) {
    uint16_t len;
    char ch;
    
    /* check if there was input, otherwise report error */
    if( fgets(buff,sz,stdin) == NULL )
	return(-1);

    len = strlen(buff);

    /* check if the last char is '\n' */
    if( buff[len-1] != '\n' ) {
	/* if it isn't, but the next char is, return */
	if( (ch = getchar()) == '\n' )
	    return len;

	/* Otherwise flush to EOF and return error code */
	while ( (ch = getchar()) != '\n' && (ch != EOF));
	return(-2);
    }

    /* If the input was less than the buffer, swap the '\n' for '\0' and return len of string */
    buff[len-1] = '\0';
    return len-1;
}

/* Allocates memory for a file struct and fills it with random number of blocks up to MAX_BLOCKS */
int generate_file( uint32_t fd, file_t * file ) {
    uint16_t num_blocks;
    uint16_t i;

    file->fd = fd;

    num_blocks = (uint16_t) 1+(rand() % (MAX_BLOCKS-1));
    file->blocks_used = 0;

    for( i=0; i<num_blocks; i++ ) {
	file->block[i] = (struct block_t *) malloc (sizeof(struct block_t));
	/* attempt to fill block with random data */
	if( fill_block(file->block[i]) == -1 ) {
	    fprintf(stderr, "unable to fill block %u in file %u\n", i, fd);
	    exit(1);
	}
	file->blocks_used++;
    }

    return(0);
}

/* Fills a block of memory with random data */
int fill_block( block_t * block ) {
    uint16_t i;

    /* fill block with random bytes */
    for( i=0; i<BLOCK_SIZE; i++ )
	//block->b[i] = (byte_t)rand();
	block->b[i] = (byte_t)0x88;

    return(0);
}

/* returns 1 if file descriptor exists in directory already */
int file_exists( dir_t * dir, uint32_t fd ) {
    if( get_file( dir, fd ) == NULL )
	return 0;
    return 1;
}
