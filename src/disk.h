bool init_disk( disk_cont_t * disk );

/* contiguous disk functions */
int write_cont( disk_cont_t * disk, file_t * file );
file_t * read_cont(  disk_cont_t * disk, uint32_t fd );
int delete_cont( disk_cont_t * disk, uint32_t fd );
void list_cont( disk_cont_t * disk );
int diskop_cont( disk_cont_t * disk, dir_t * dir, char * filename );

int get_start_block( disk_cont_t * disk, int size ); 
bool enough_space( disk_cont_t * disk, int size, int * index);
bool copy_block( block_t * block_out, block_t * block_in );

/* noncontiguous disk functions */
int write_noncont( disk_noncont_t * disk, file_t * file );
file_t * read_noncont( disk_noncont_t * disk, uint32_t fd );
int delete_noncont( disk_noncont_t * disk, uint32_t fd );
void list_noncont( disk_noncont_t * disk );
int diskop_noncont( disk_noncont_t * disk, dir_t * dir, char * filename );

bool init_noncont_disk( disk_noncont_t * disk );
block_t * get_block_addr( disk_noncont_t * disk );
int get_index_from_block_addr( block_t * base, block_t * addr );

/* noncontiguous disk linked list structure */
noncont_entry * get_noncont_entry( disk_noncont_t * disk, uint32_t fd );
int remove_noncont_entry( disk_noncont_t * disk, uint32_t fd );
int add_noncont_entry( disk_noncont_t * disk, noncont_entry * entry );

/* run disk operations listed in file for contiguous disk */
int diskop_cont( disk_cont_t * disk, dir_t * dir, char * filename ) {
    FILE * fin;
    char * line = NULL;
    size_t len;
    ssize_t read;
    uint32_t count;
    char ** prop;
    int ret;
    int fd;
    file_t * file;
    
    if( (fin = fopen( filename, "r" )) == NULL ) {
	fprintf(stderr, "unable to read file\n");
	return(-1);
    }
    

    printf("File\t\tOperation\tStartblock\tlength\t\tstatus\n");
    while( ( read = getline( &line, &len, fin )) != -1 ) {
	get_args( line, &count, &prop );

	if((fd = atoi(prop[0])) == 0 ){
	    fprintf(stderr, "invalid identifier\n");
	    return(-1);
	}

	// WRITE TO DISK	
	if( strcmp( prop[1], "W" ) == 0 ) {
	    ret = write_cont( disk, (file = get_file(dir, fd)));
	    if( ret >= 0 )
		printf("%d\t\tW\t\t%d\t\t%d\t\t%s\n", fd, ret, file->blocks_used, "SUCCESS"); 
	    else if( ret == DISK_FULL ) {
		printf("%d\t\tW\t\t-\t\t-\t\tDISK FULL\n", fd);
	    } else {
		printf("%d\t\tW\t\t-\t\t-\t\tFILE DOES NOT EXIST\n", fd);
	    }		
	}

	// READ FROM DISK
	if( strcmp( prop[1], "R" ) == 0 ) {
	    file = read_cont( disk, fd );
	    if( file == NULL ) {
		printf("%d\t\tR\t\t-\t\t-\t\tFILE DOES NOT EXIST\n", fd);
	    }
	    else {
		printf("%d\t\tR\t\t-\t\t%d\t\t%s\n", fd, file->blocks_used, "SUCCESS");
	    }
	}

	// DELETE FROM DISK
	if( strcmp( prop[1], "D" ) == 0 ) {
	    ret = delete_cont( disk, fd );
	    if( ret == 0 ) {
		printf("%d\t\tD\t\t-\t\t%d\t\t%s\n", fd, get_file(dir, fd)->blocks_used, "SUCCESS");
	    }
	    else {
		printf("%d\t\tR\t\t-\t\t-\t\tFILE DOES NOT EXIST\n", fd);
	    }
	}

    }
    fclose(fin);
    return 0;
}

/* run disk operations listed in file for contiguous disk */
int diskop_noncont( disk_noncont_t * disk, dir_t * dir, char * filename ) {
    FILE * fin;
    char * line = NULL;
    size_t len;
    ssize_t read;
    uint32_t count;
    char ** prop;
    int ret;
    int fd;
    int i;
    file_t * file;
    noncont_entry * ent;

    if( (fin = fopen( filename, "r" )) == NULL ) {
	fprintf(stderr, "unable to read file\n");
	return(-1);
    }
    printf("File\t\tOperation\tblocks\t\t\tlength\t\tstatus\n");
    while( ( read = getline( &line, &len, fin )) != -1 ) {
	get_args( line, &count, &prop );

	if((fd = atoi(prop[0])) == 0 ){
	    fprintf(stderr, "invalid identifier\n");
	    return(-1);
	}

	// WRITE TO DISK	
	if( strcmp( prop[1], "W" ) == 0 ) {
	    ret = write_noncont( disk, (file = get_file(dir, fd)));
	    if( ret == 0 ) {
		ent = get_noncont_entry( disk, fd );
		printf("%d\t\tW\t\t", fd); 
		for( i=0; i<ent->size; i++ ) {
		    printf("%d,", get_index_from_block_addr(disk->block, ent->block[i]));
		}

		printf("\t%d\t%s\n", file->blocks_used, "SUCCESS" );
	    }
	    else if( ret == DISK_FULL ) {
		printf("%d\t\tW\t-\t-\tDISK FULL\n", fd);
	    } else {
		printf("%d\t\tW\t-\t-\tFILE DOES NOT EXIST\n", fd);
	    }		
	}

	// READ FROM DISK
	if( strcmp( prop[1], "R" ) == 0 ) {
	    file = read_noncont( disk, fd );
	    if( file == NULL ) {
		printf("%d\t\tR\t\t-\t\t-\t\tFILE DOES NOT EXIST\n", fd);
	    }
	    else {
		ent = get_noncont_entry( disk, fd );
		printf("%d\tR\t\t", fd); 
		for( i=0; i<ent->size; i++ ) {
		    printf("%d,", get_index_from_block_addr(disk->block, ent->block[i]));
		}

		printf("\t%d\t%s\n", file->blocks_used, "SUCCESS" );
	    }
	}
	// DELETE FROM DISK
	if( strcmp( prop[1], "D" ) == 0 ) {

	    ret = delete_noncont( disk, fd );
	    if( ret == 0 ) {
		printf("%d\t\tD\t\t-\t\t%d\t\t%s\n", fd, get_file(dir, fd)->blocks_used, "SUCCESS");
	    }
	    else {
		printf("%d\t\tR\t\t-\t\t-\t\tFILE DOES NOT EXIST\n", fd);
	    }


	}

    }
    fclose(fin);
    return 0;
}


/* initialize the disk */
bool init_disk( disk_cont_t * disk ) {
    //int i;
    /*    for(i=0; i<DISK_SIZE; i++)
	disk->table[i] = (cont_table_entry *) malloc (sizeof(cont_table_entry));
    */
    //    disk->block = (block_t *) malloc (DISK_SIZE * sizeof(block_t));
    
    return TRUE;
}

/* initialized a noncont disk */
bool init_noncont_disk( disk_noncont_t * disk ) {
    int i;
    disk->first = NULL;
    disk->blocks_used = 0;
    for(i=0; i<DISK_SIZE; i++ ) {
	disk->used[i] = FALSE;
    }
    return TRUE;
}

/* list all entries in disk table */
void list_cont( disk_cont_t * disk ) {
    int i,j;
    printf("fd\tstart\tend\tsize\n");
    for( i=0; i<DISK_SIZE; i++ ) {
	if( disk->table[i] != NULL ) {
	    /* print contents and advance index */
	    printf("%d\t%d\t%d\t%d\n", disk->table[i]->fd, disk->table[i]->start_block, disk->table[i]->start_block+disk->table[i]->size-1,disk->table[i]->size);

	    i=i+disk->table[i]->size-1;
	}
	
    }
    
    printf("Disk Visualization:\n");
    for( i=0; i<DISK_SIZE; i++ ) {
	if( disk->table[i] != NULL ) {
	    /* print contents and advance index */
	    for(j=0; j<disk->table[i]->size; j++) {
		printf("#");
	    }
	    i=i+disk->table[i]->size-1;
	}
	else printf("-");
    }
    printf("\n");

}

/* return -1 if failure occurs */
int write_cont( disk_cont_t * disk, file_t * file ) {
    int start_block,f,d;
    cont_table_entry * ent;

    if( file == NULL ) {
	fprintf(stderr, "File does not exist\n");
	return(FILE_DOES_NOT_EXIST);
    }
    // find contiguous space big enough
    // get start block    
    if( (start_block = get_start_block( disk, file->blocks_used)) == -1 ) {
	fprintf(stderr, "failed to write file %d, no available space\n", file->fd);
	return(DISK_FULL);
    }

    // copy blocks in
    for( f=0, d=start_block; f < file->blocks_used; f++, d++ ) {
	copy_block( &disk->block[d], file->block[f] );
    }
    
    // update disk_cont_table
    disk->table[start_block] = (cont_table_entry *) malloc (sizeof(cont_table_entry));
    ent = disk->table[start_block];
    ent->fd = file->fd;
    ent->start_block = start_block;
    ent->size = file->blocks_used;
    return start_block;
}

/* attempt to delete a contiguous block */
int delete_cont( disk_cont_t * disk, uint32_t fd ) {
    int i;
    for( i=0; i<DISK_SIZE; i++ ) {
	if( disk->table[i] != NULL ) {
	    if( disk->table[i]->fd == fd ) {
		disk->table[i] = NULL;
		return 0;
	    }
	    else
		i=i+disk->table[i]->size-1;
	}
    }
    return(FILE_DOES_NOT_EXIST);
    
}

/* return pointer to file after reading from disk */
file_t * read_cont( disk_cont_t * disk, uint32_t fd ) {
    file_t * file;
    int i, d, f;
    for( i=0; i<DISK_SIZE; i++ ) {
	if( disk->table[i] != NULL ) {
	    if( disk->table[i]->fd == fd ) {
		// allocate file_t and return file_t
		file = (file_t *) malloc (sizeof(file_t));
		file->fd = disk->table[i]->fd;
		file->blocks_used = disk->table[i]->size;
		for( f=0, d=i; f < disk->table[i]->size; f++,d++ ) {
		    file->block[f] = (struct block_t *) malloc (sizeof(struct block_t));
		    copy_block( file->block[f], &disk->block[d] );
		}		
		return file;
	    }
	    else
		i=i+disk->table[i]->size-1;
	}
    }
    return(NULL);    

}

/* write file to noncont disk */
int write_noncont( disk_noncont_t * disk, file_t * file ) {
    noncont_entry * ent;
    int i;
    block_t * block_addr;

    if( file == NULL ) {
	//	fprintf(stderr, "File does not exist\n");
	return(FILE_DOES_NOT_EXIST);
    }

    /* Check if there's enough space on disk */
    if( (DISK_SIZE-disk->blocks_used) < file->blocks_used ) {
	fprintf(stderr, "Not enough space on disk for file %d\n", file->fd);
	return(DISK_FULL);
    }

    ent = (noncont_entry *) malloc (sizeof(noncont_entry));    
    ent->fd = file->fd;
    ent->size = file->blocks_used;
    ent->block = malloc (sizeof(block_t *) * ent->size);

    /* first attempt to find enough blocks */
    for( i=0; i<ent->size; i++ ) {
	// find space on disk, point to it
	block_addr = get_block_addr( disk );
	ent->block[i] = block_addr;
    }

    /* if enough blocks were found, write all to disk */
    for( i=0; i<ent->size; i++ ) {
	copy_block( ent->block[i], file->block[i] );		
    }
    
    // add entry to disk entry list    
    if( add_noncont_entry( disk, ent ) == -1 ) {
	fprintf(stderr, "unable to add new entry");
	return(-1);
    }
    return 0;
}

/* delete file with file descripton fd from disk */
int delete_noncont( disk_noncont_t * disk, uint32_t fd ) {
    noncont_entry * ent;
    int i, ind;
    // get entry
    if( (ent = get_noncont_entry( disk, fd )) == NULL ) {
	fprintf(stderr, "file not found\n");
	return(FILE_DOES_NOT_EXIST);
    }

    // change all used block flags to FALSE
    for( i=0; i<ent->size; i++ ) {
	ind = get_index_from_block_addr(disk->block, ent->block[i]);
	disk->used[ind] = FALSE;
    }

    // remove entry
    remove_noncont_entry( disk, fd );
    return 0;
}

/* read file from disk with file descriptor fd */
file_t * read_noncont( disk_noncont_t * disk, uint32_t fd ) {
    noncont_entry * ent;
    file_t * file;
    int f;

    // get entry 
    if( (ent = get_noncont_entry( disk, fd )) == NULL ) {
	fprintf(stderr, "file not found\n");
	return(NULL);
    }

    // allocate file_t and return file_t
    file = (file_t *) malloc (sizeof(file_t));
    file->fd = ent->fd;
    file->blocks_used = ent->size;
    for( f=0; f < ent->size; f++ ) {
	file->block[f] = (struct block_t *) malloc (sizeof(struct block_t));
	copy_block( file->block[f], ent->block[f] );
    }		
    return file;

    return NULL;
}

/* list contents of noncontiguous disk */
void list_noncont( disk_noncont_t * disk ) {
    int i;
    noncont_entry * it;
    it = disk->first;
    printf("blocks used:%d\n", disk->blocks_used);
    printf("Disk Visualization:\n");
    for( i=0; i<DISK_SIZE; i++ ) {
	if(disk->used[i])
	    printf("#");
	else
	    printf("-");
    }
    printf("\n");
    
    while(it != NULL) {
	printf("file: %d\nsize: %d\n", it->fd, it->size);

	printf("Blocks: ");

	for( i=0; i<it->size; i++ ) {
	    printf("%d ", get_index_from_block_addr(disk->block, it->block[i]));
	}
	it = it->next;
	printf("\n\n");
    }
}

/* get index from address of block and address of blocks */
int get_index_from_block_addr( block_t * base, block_t * addr ) {
    int index;
    index = (int)(addr-base);
    return index;
}

/* find what block to start writing contiguously to */
int get_start_block( disk_cont_t * disk, int size ) {
    int i;

    for( i=0; i<DISK_SIZE; i++ ) {
	if( disk->table[i] != NULL ) {
	    i=i+disk->table[i]->size-1;
	}
	else if(enough_space(disk, size, &i))
	    return(i);
    }
    return(-1);
}

/* see if there's enough space at the address specified */
bool enough_space( disk_cont_t * disk, int size, int * index) {
    int i, end;
    end = *index+size;
    if( end > DISK_SIZE ) return(FALSE);
    for( i=*index; i<end; i++ ) {
	if(disk->table[i] != NULL ) {
	    *index = i;
	    return(FALSE);
	}
    }

    return(TRUE);
}

bool copy_block( block_t * block_out, block_t * block_in ) {
    int i;
    for( i=0; i<BLOCK_SIZE; i++ ) {
	block_out->b[i] = block_in->b[i];
    }
    return(TRUE);
}

// get a random block address 
block_t * get_block_addr( disk_noncont_t * disk ) {
    int block_id, i;
    block_id = (int) rand() % DISK_SIZE;
    for(i=0; i<DISK_SIZE; i++ ) {
	if(disk->used[block_id] == FALSE) {
	    disk->used[block_id] = TRUE;
	    disk->blocks_used++;
	    return(&(disk->block[block_id]));	    
	}
	block_id = (block_id + 1) % DISK_SIZE;
    }
    return NULL;
}

/* add a new noncont_entry */
int add_noncont_entry( disk_noncont_t * disk, noncont_entry * entry ) {
    entry->next = disk->first;
    disk->first = entry;
    return 0;
}

/* remove noncont_entry */
int remove_noncont_entry( disk_noncont_t * disk, uint32_t fd ) {
    noncont_entry * it, * last;
    it = disk->first;
    if( it->fd == fd ) {
	disk->first = it->next;
	free(it);
	return 0;
    }	

    last = it;
    it = it->next;
    while( it != NULL ) {
	if( it->fd == fd ) {
	    last->next = it->next;
	    free(it);
	    return 0;
	}	
	last = it;
	it = it->next;
    }
    return(-1);
}

/* retrieves pointer to noncont_entry */
noncont_entry * get_noncont_entry( disk_noncont_t * disk, uint32_t fd ) {
    noncont_entry * it;
    it = disk->first;
    while( it != NULL ) {
	if( it->fd == fd ) {
	    return it;
	}
	it = it->next;
    }
    return(NULL);
}

