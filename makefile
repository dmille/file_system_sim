CC=gcc
CFLAGS = -Wall -c -g
SRCS = src/fs.c
OBJS = $(SRCS:.c=.o)
EXEC = fs

all: $(SRCS) $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
clean:
	rm $(OBJS) $(EXEC) *~ *#
